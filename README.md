# Calamares Settings for the Mobian Installer
This repo serves as the location for our [Calamares](https://github.com/calamares/calamares)
[extensions](https://github.com/calamares/calamares-extensions) configuration.

 - We use calamares as it has the benefit of being the same base installer for
   ourselves and pmOS (and perhaps others).
 - All of the Mobian installer UI and options come from the calamares-extensions
   package (lightly patched upstream code).

This repo is the configuration to make calamares use the right modules and
install the system in the way Mobian users expect. Modifications to install
scripts and config should be done here, work on the broader installer is best
placed in upstream calamares-extensions.

## Currently supported devices:
* Pine64 PinePhone
* Pine64 PinePhone Pro

## Adding a new device
To add a new device the following (approximate) steps should be followed:
1. Ensure that the `$device_model_name` from `scripts/calamares-mobian-ondev`
   correctly recognises the new device. This will show up on the front page
   of the installer.
2. Ensure that the `booted_from_disk` and `part_*` variables are correctly
   set at the top of `scripts/calamares-mobian-ondev`. These should map to:
   - `booted_from_disk`: The disk (not partition) that the installer booted from.
   - `part_install`: The installers root portiton.
   - `part_target`: The target root partition on the installer disk (should
     be the second partition).
   - `part_internal_target`: The internal storage disk (not partition) of the device if
     the device was booted from SD card (or other external media). If booted
     from internal media, this must be an empty string.
3. If the device cannot boot with a large initramfs configure miniramfs in
   `mobian-recipes` and ensure it is rebuilt in `scripts/calamares-install-bootloader`.
   Currently this is done for the Pine64 PinePhone Pro.
4. If the device requires a u-boot install or similar this should be run
   in `scripts/prepare-internal-storage`'s `partitions_create()` function.
   Where possible, a dedicated storage install of U-Boot such as Tow-Boot should be used.

